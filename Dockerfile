FROM openjdk:17-alpine
WORKDIR /home/app
ENV TZ="America/Argentina/Buenos_Aires"
COPY ./build/docker/main/layers/libs /home/app/libs
COPY ./build/docker/main/layers/resources /home/app/resources
COPY ./build/docker/main/layers/application.jar /home/app/application.jar
RUN apk add tzdata &&\
    echo "America/Argentina/Buenos_Aires" > /etc/timezone &&\
    mkdir logs && mkdir infra
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/home/app/application.jar"]
