# Aplicación Ensolvers

## Tecnologías:

- Java 17
- Gradle
- Docker
- Docker-compose
- Micronaut Framework
- PostgreSQL
- Lombok

Para correr la aplicación, se debe tener instalado en el sistema: java 17, docker y docker-compose. El servidor web y la base de datos postgreSQL se descargan como imágenes docker.

## Levantar la aplicación

Para levantar la aplicación se debe correr el script ubicado en el directorio root del proyecto.

./runApp.sh

Este script creará una carpeta llamada "infra" dentro del proyecto, copiara varios archivos de configuración y, con docker-compose, levantará dos servicios, uno es el servidor web del backend de la aplicación y otro es un servidor de base de datos PostgreSQL.


## Comunicación con la aplicación:

- curl -v -X POST -H  "Content-Type:application/json"  -d '{ "texto" : "comprar fernet", "seleccionado" : false, "folderName" : "root_folder" }' http://localhost:8080/item
- curl -v -X POST -H  "Content-Type:application/json"  -d '{ "texto" : "comprar cerveza", "seleccionado" : false, "folderName" : "root_folder" }' http://localhost:8080/item
- curl http://localhost:8080/item/1
- curl http://localhost:8080/item
- curl http://localhost:8080/root_folder/item/
- curl http://localhost:8080/otro_folder_distinto/item/
- curl -v -X PUT -H  "Content-Type:application/json"  -d '{ "id": "1", "texto" : "comprar gin", "seleccionado" : false, "folderName" : "root_folder" }' http://localhost:8080/item/
- curl -v -X PUT -H  "Content-Type:application/json"  -d '{ "id": "1", "seleccionado" : true }' http://localhost:8080/item/mark


## Micronaut 3.3.4 Documentation

- [User Guide](https://docs.micronaut.io/3.3.4/guide/index.html)
- [API Reference](https://docs.micronaut.io/3.3.4/api/index.html)
- [Configuration Reference](https://docs.micronaut.io/3.3.4/guide/configurationreference.html)
- [Micronaut Guides](https://guides.micronaut.io/index.html)
