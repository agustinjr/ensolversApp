echo "creo los directorios para los volúmenes de base de datos si no existen"
mkdir -p ./infra/data
mkdir -p ./infra/script
echo "empaqueto la aplicación"
./gradlew assemble
echo "copio los archivos de configuración"
cp ./src/main/resources/db/init.sql ./infra/script/
cp ./src/main/resources/application-produccion.yml ./infra/
echo "construyo la imagen y levanto la infraestructura necesaria"
docker-compose -f docker-compose-build.yml up --remove-orphans
