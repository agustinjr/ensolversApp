package rios.agustin.DTO;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Introspected
public class FolderDTO {


    private Long id;

    @NotBlank(message = "The Name of the folder can't be empty")
    String nombre;
}
