package rios.agustin.DTO;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Introspected
public class ItemDTO {

    private Long id;

    @NotBlank(message = "The text can't be empty")
    private String texto;

    private Boolean seleccionado;

    @Size(max = 30, message = "The name of the folder can't exceed ")
    private String folderName;
}
