package rios.agustin.controller;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import rios.agustin.DTO.FolderDTO;
import rios.agustin.DTO.ItemDTO;
import rios.agustin.service.FolderService;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Controller("/")
public class FolderController {

    private final FolderService folderService;

    public FolderController(FolderService folderService) {
        this.folderService = folderService;
    }

    // curl -v -X POST -H  "Content-Type:application/json"  -d '{ "nombre" : "otroFolder"}' http://localhost:8080/folder
    @Produces(MediaType.APPLICATION_JSON)
    @Post("/folder")
    public FolderDTO saveFolder(@Valid @Body FolderDTO folderDTO ) throws Exception {
        return folderService.saveFolder(folderDTO);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Get("/folder/{id}")
    FolderDTO getFolder(@NotBlank Long id) throws Exception {
        return folderService.getFolder(id);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Get("/folder/{name}")
    FolderDTO getFolder(@NotBlank String name) throws Exception {
        return folderService.getFolder(name);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Get("/folder")
    public List<FolderDTO> getFolders() {
        //Devuelve los items del folder root
        return folderService.getFolders();
    }

    // curl -v -X PUT -H  "Content-Type:application/json"  -d '{ "id": "2" , "nombre" : "otroFoldersss"}' http://localhost:8080/folder
    @Produces(MediaType.APPLICATION_JSON)
    @Put("/folder")
    FolderDTO updateFolder(@Body @Valid  FolderDTO folderDTO ) throws Exception {

        return folderService.updateFolder(folderDTO);

    }

    // curl -X DELETE http://localhost:8080/folder/7
    @Produces(MediaType.APPLICATION_JSON)
    @Delete("/folder/{id}")
    void deleteFolderById( Long id) throws Exception {
        folderService.deleteFolder(id);
    }
}
