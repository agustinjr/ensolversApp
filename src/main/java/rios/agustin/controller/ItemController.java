package rios.agustin.controller;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import rios.agustin.DTO.ItemDTO;
import rios.agustin.service.IItemService;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Controller("/")
public class ItemController {
 
	private final IItemService itemService;

	public ItemController(IItemService itemService) {
		this.itemService = itemService;
	}

	// curl -v -X POST -H  "Content-Type:application/json"  -d '{ "texto" : "comprar fernet", "seleccionado" : false, "folderName" : "otro_folder" }' http://localhost:8080/item
	@Produces(MediaType.APPLICATION_JSON)
	@Post("/item")
	public ItemDTO saveItem(@Valid @Body ItemDTO itemDTO ) throws Exception {
		return itemService.saveItem(itemDTO);
	}

	// curl http://localhost:8080/item/1
	@Produces(MediaType.APPLICATION_JSON)
	@Get("/item/{id}")
	ItemDTO getItem(@NotBlank Long id) throws Exception {
		return itemService.getItem(id);
	}

	// curl http://localhost:8080/item
	@Produces(MediaType.APPLICATION_JSON)
	@Get("/item")
	public List<ItemDTO> getItems() {
		//Devuelve los items del folder root
		return itemService.getItems();
	}

	// curl http://localhost:8080/root_folder/item/
	@Produces(MediaType.APPLICATION_JSON)
	@Get("/{folderName}/item")
	List<ItemDTO> getItemOfFolder(@NotBlank String folderName) throws Exception {
		return itemService.getItems(folderName);
	}

	// curl -v -X PUT -H  "Content-Type:application/json"  -d '{ "id": "3", "texto" : "COMPRAR GIN", "seleccionado" : true, "folderName" : "root_folder" }' http://localhost:8080/item/
	@Produces(MediaType.APPLICATION_JSON)
	@Put("/item")
	ItemDTO updateItem(@Body @Valid  ItemDTO itemDTO ) throws Exception {

		return itemService.updateItem(itemDTO);

	}

	// curl -v -X PUT -H  "Content-Type:application/json"  -d '{ "id": "3", "texto" : "COMPRAR GIN", "seleccionado" : true, "folderName" : "root_folder" }' http://localhost:8080/item/
	@Produces(MediaType.APPLICATION_JSON)
	@Put("/item/mark")
	ItemDTO markItem(@Body ItemDTO itemDTO ) throws Exception {

		return itemService.markItem(itemDTO);

	}

	// curl -X DELETE http://localhost:8080/item/3
	@Produces(MediaType.APPLICATION_JSON)
	@Delete("/item/{id}")
	void deleteItem(@QueryValue("id") Long id) {
		itemService.deleteItem(id);
	}
}