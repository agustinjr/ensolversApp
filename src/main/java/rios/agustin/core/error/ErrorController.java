package rios.agustin.core.error;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Error;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

@Controller
@Slf4j
public class ErrorController {

    @Error(global = true)
    public HttpResponse<?> error(HttpRequest request, AppException e) {

        log.error("ERROR de Validación", e.getMessage());
        ErrorDTO error = new ErrorDTO();
        error.getMensajes().add(e.getMessage());


        return HttpResponse
                .status(HttpStatus.valueOf(HttpStatus.BAD_REQUEST.getCode()))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .body(error);
    }

    @Error(global = true)
    public HttpResponse<?> error(HttpRequest request, ConstraintViolationException e) {

        log.error("ERROR de Validación", e.getMessage());

        ErrorDTO error = new ErrorDTO();
        for(ConstraintViolation<?> c : e.getConstraintViolations()) {
            log.debug("Constraint: "+c.getMessage());
            error.getMensajes().add(c.getMessage());
        }


        return HttpResponse
                .status(HttpStatus.valueOf(HttpStatus.BAD_REQUEST.getCode()))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .body(error);
    }

    @Error(global = true)
    public HttpResponse<?> error(HttpRequest request, Exception e) {

        log.error("ERROR de Validación", e.getMessage());
        e.printStackTrace();
        ErrorDTO error = new ErrorDTO();
        error.getMensajes().add("Error Interno del Servidor");


        return HttpResponse
                .status(HttpStatus.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.getCode()))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .body(error);
    }

}
