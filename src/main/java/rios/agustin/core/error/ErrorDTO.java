package rios.agustin.core.error;

import java.util.ArrayList;
import java.util.List;

public class ErrorDTO {

    private List<String> mensajes;

    public ErrorDTO() {
        mensajes = new ArrayList<>();
    }

    public List<String> getMensajes() {
        return mensajes;
    }

    public void setMensajes(List<String> mensajes) {
        this.mensajes = mensajes;
    }

    @Override
    public String toString() {
        return "ErrorDTO(errores=" + this.getMensajes() + ")";
    }
}