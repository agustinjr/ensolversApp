package rios.agustin.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "folder")
@Getter
@Setter
public class Folder extends BaseEntity {

    @Column(name = "nombre")
    String nombre;

}
