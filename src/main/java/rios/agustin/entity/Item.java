package rios.agustin.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "item")
@Getter
@Setter
public class Item extends BaseEntity {

    @Column(name = "texto")
    String texto;

    @Column(name = "seleccionado")
    private Boolean seleccionado;

    @ManyToOne
    @JoinColumn(name = "idfolder")
    private Folder folder;
}
