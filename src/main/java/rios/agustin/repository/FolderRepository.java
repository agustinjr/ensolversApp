package rios.agustin.repository;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;
import rios.agustin.entity.Folder;

import java.util.Optional;

@Repository
public interface FolderRepository extends CrudRepository<Folder, Long> {

    Optional<Folder> findByNombre(String nombre);

}
