package rios.agustin.service;

import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import rios.agustin.DTO.FolderDTO;
import rios.agustin.core.error.AppException;
import rios.agustin.entity.Folder;
import rios.agustin.entity.Item;
import rios.agustin.repository.FolderRepository;
import rios.agustin.repository.ItemRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Singleton
@Slf4j
public class FolderService implements IFolderService {

    private final ItemRepository itemRepository;
    private final FolderRepository folderRepository;

    public FolderService(ItemRepository itemRepository, FolderRepository folderRepository) {
        this.itemRepository = itemRepository;
        this.folderRepository = folderRepository;
    }

    @Override
    public FolderDTO saveFolder(FolderDTO folderDTO) throws AppException {
        log.debug("Entro a saveFolder()");
        Folder folder = new Folder();
        folder.setNombre(folderDTO.getNombre());
        try {
            folder = folderRepository.save(folder);
        }catch(Exception e) {
            log.error("Error guardando el folder");
            e.printStackTrace();
            throw new AppException("Name of folder alredy exists");
        }
        folderDTO.setId(folder.getId());
        return folderDTO;
    }

    @Override
    public List<FolderDTO> getFolders() {
        return StreamSupport.stream(folderRepository.findAll().spliterator(),false).map(this::toDTO).collect(Collectors.toList());
    }

    @Override
    public FolderDTO getFolder(Long id) throws AppException {
        Folder folder = folderRepository.findById(id).orElseThrow(() ->new AppException("The folder with id "+id+" does not exist"));
        return toDTO(folder);
    }

    @Override
    public FolderDTO getFolder(String folderName) throws AppException {
        Folder folder = folderRepository.findByNombre(folderName).orElseThrow(() ->new AppException("The folder with name "+folderName+" does not exist"));
        return toDTO(folder);
    }

    @Override
    public FolderDTO updateFolder(FolderDTO folderDTO) throws AppException {
        log.debug("Entro a updateFolder()");
        Folder folder = folderRepository.findById(folderDTO.getId()).orElseThrow(() ->new AppException("The folder with id "+folderDTO.getId()+" does not exist"));
        folder.setNombre(folderDTO.getNombre());
        folder = folderRepository.update(folder);
        return toDTO(folder);

    }

    @Override
    @Transactional
    public void deleteFolder(Long id) throws AppException {
        log.debug("Entro a deleteFolder()");
        if(id == 1L) {
            throw new AppException("Can't delete root folder");
        }
        Folder folder = folderRepository.findById(id).orElseThrow(() ->new AppException("The folder with id "+id+" does not exist"));
        List<Item> items = itemRepository.findByFolder(folder);
        if(!items.isEmpty()) {
            log.debug("Elimino "+items.size()+" items asociados al folder");
            itemRepository.deleteAll(items);
        }
        folderRepository.delete(folder);
    }

    @Override
    @Transactional
    public void deleteFolder(String folderName) throws AppException {
        Folder folder = folderRepository.findByNombre(folderName).orElseThrow(() ->new AppException("The folder with name "+folderName+" does not exist"));
        List<Item> items = itemRepository.findByFolder(folder);
        if(items.isEmpty()) {
            return;
        }
        itemRepository.deleteAll(items);
        folderRepository.delete(folder);
    }

    private FolderDTO toDTO(Folder folder) {
        FolderDTO folderDTO = new FolderDTO();
        folderDTO.setNombre(folder.getNombre());
        folder.setId(folder.getId());
        return folderDTO;
    }
}
