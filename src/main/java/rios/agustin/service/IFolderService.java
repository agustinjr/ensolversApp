package rios.agustin.service;

import rios.agustin.DTO.FolderDTO;

import java.util.List;

public interface IFolderService {

    public FolderDTO saveFolder(FolderDTO folderDTO) throws Exception;

    public List<FolderDTO> getFolders();

    public FolderDTO getFolder(Long id) throws Exception;

    public FolderDTO getFolder(String folderName) throws Exception;

    public FolderDTO updateFolder(FolderDTO folderDTO) throws Exception;

    public void deleteFolder(Long id) throws Exception;

    public void deleteFolder(String folderName) throws Exception;



}
