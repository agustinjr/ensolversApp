package rios.agustin.service;

import rios.agustin.DTO.ItemDTO;

import java.util.List;

public interface IItemService {

    public ItemDTO saveItem(ItemDTO itemDTO) throws Exception;

    public List<ItemDTO> getItems();

    public List<ItemDTO> getItems(String folderName)  throws Exception;

    public ItemDTO getItem(Long id) throws Exception;

    public ItemDTO updateItem(ItemDTO itemDTO) throws Exception;

    public void deleteItem(Long id);

    public ItemDTO markItem(ItemDTO itemDTO) throws Exception;

}
