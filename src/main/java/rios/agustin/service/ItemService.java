package rios.agustin.service;

import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import rios.agustin.DTO.ItemDTO;
import rios.agustin.core.error.AppException;
import rios.agustin.entity.Folder;
import rios.agustin.entity.Item;
import rios.agustin.repository.FolderRepository;
import rios.agustin.repository.ItemRepository;

import java.util.List;
import java.util.stream.Collectors;

@Singleton
@Slf4j
public class ItemService implements IItemService {

    private final ItemRepository itemRepository;
    private final FolderRepository folderRepository;

    private final String ROOTFOLDER = "root_folder";

    public ItemService(ItemRepository itemRepository, FolderRepository folderRepository) {
        this.itemRepository = itemRepository;
        this.folderRepository = folderRepository;
    }

    @Override
    public ItemDTO saveItem(ItemDTO itemDTO) throws AppException {

        log.debug("Entro a saveItem()");
        Item item = new Item();
        Folder folder;
        if(itemDTO.getFolderName() == null || itemDTO.getFolderName().isBlank()) {
            folder = folderRepository.findByNombre(ROOTFOLDER).get(); //se que existe en la base
        } else {
            folder = folderRepository.findByNombre(itemDTO.getFolderName()).orElseThrow(() -> new AppException("The folder with name "+ itemDTO.getFolderName()+" does not exist"));
        }
        item.setFolder(folder);
        item.setTexto(itemDTO.getTexto());
        item.setSeleccionado(itemDTO.getSeleccionado());
        item = itemRepository.save(item);

        itemDTO.setId(item.getId());
        return itemDTO;
    }

    @Override
    public List<ItemDTO> getItems() {
        log.debug("Entro a getItems()");
        Folder rootFolder = folderRepository.findByNombre(ROOTFOLDER).get();
        return itemRepository.findByFolder(rootFolder).stream().map(this::toItemDTO).collect(Collectors.toList());
    }

    @Override
    public List<ItemDTO> getItems(String folderName) throws  AppException {
        log.debug("Entro a getItems("+folderName+")");
        Folder folder = folderRepository.findByNombre(folderName).orElseThrow(() -> new AppException("The folder with name "+folderName+" does not exist"));
        return itemRepository.findByFolder(folder).stream().map(this::toItemDTO).collect(Collectors.toList());
    }

    @Override
    public ItemDTO getItem(Long id) throws AppException {
        Item item =  itemRepository.findById(id).orElseThrow(()-> new AppException("El item con id "+ id +" no existe"));
        return toItemDTO(item);
    }

    @Override
    public ItemDTO updateItem(ItemDTO itemDTO) throws AppException {

        Item item = itemRepository.findById(itemDTO.getId()).orElseThrow(()-> new AppException("El item con id "+ itemDTO.getId() +" no existe"));
        item.setSeleccionado(itemDTO.getSeleccionado());
        item.setTexto(itemDTO.getTexto());
        item.setId(itemDTO.getId());

        Folder folder = folderRepository.findByNombre(itemDTO.getFolderName()).orElseThrow(() -> new AppException("El Folder "+ itemDTO.getFolderName()+" no existe"));
        item.setFolder(folder);
        item = itemRepository.update(item);

        return toItemDTO(item);
    }

    @Override
    public void deleteItem(Long id) {
        itemRepository.deleteById(id);
    }

    @Override
    public ItemDTO markItem(ItemDTO itemDTO) throws Exception {
        log.debug("Entro a markItem()");

        Item item = itemRepository.findById(itemDTO.getId()).orElseThrow(()-> new AppException("El item con id "+ itemDTO.getId() +" no existe"));

        if(itemDTO.getSeleccionado() == null) {
            throw new AppException("The item with id "+ itemDTO.getId() +" must have 'selected' field not null");
        }
        if(itemDTO.getSeleccionado().equals(item.getSeleccionado())) {
            return toItemDTO(item);
        }
        item.setSeleccionado(itemDTO.getSeleccionado());
        item = itemRepository.update(item);

        return toItemDTO(item);
    }

    private Item toItemEntity(ItemDTO itemDTO) {
        Item item = new Item();
        item.setSeleccionado(itemDTO.getSeleccionado());
        item.setTexto(itemDTO.getTexto());
        itemDTO.setId(itemDTO.getId());
        return item;
    }

    private ItemDTO toItemDTO(Item item) {
        ItemDTO itemDTO = new ItemDTO();
        itemDTO.setId(item.getId());
        itemDTO.setTexto(item.getTexto());
        item.setSeleccionado(item.getSeleccionado());
        itemDTO.setFolderName(item.getFolder().getNombre());
        return itemDTO;
    }
}
