create database agustin;
create user agustin with encrypted password 'agustin';
grant all privileges on database agustin to agustin;
ALTER DATABASE agustin OWNER TO agustin;

\c agustin agustin;

CREATE TABLE folder (
    id bigserial primary key,
    nombre varchar(60),
    version bigint default 0
   );

insert into folder (nombre) values ('root_folder');

CREATE TABLE item (
        id bigserial primary key,
        version bigint default 0,
        texto text not null,
        seleccionado bool not null,
        idfolder bigint not null
);

ALTER TABLE item ADD CONSTRAINT  item_fk_idFolder FOREIGN KEY (idFolder) REFERENCES folder(id);
alter table folder add constraint nombre_unique unique (nombre);
--drop table folder;
--drop table item;
